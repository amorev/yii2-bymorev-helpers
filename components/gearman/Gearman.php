<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 06.06.16
 * Time: 16:32
 */

namespace Bymorev\components\gearman;

use Bymorev\components\gearman\exceptions\GearmanException;
use Bymorev\components\gearman\helpers\JobNames;
use Bymorev\components\gearman\interfaces\WorkerInterface;
use GearmanClient;
use GearmanWorker;
use yii\base\Component;
use yii\helpers\Json;

class Gearman extends Component
{
	/**
	 * @var GearmanClient
	 */
	public $gearman_client;

	/**
	 * @var GearmanWorker
	 */
	public $gearman_worker;

	public $jobNamesPrefix = null;

	/**
	 * @var bool
	 */
	public $dev_mode = FALSE;

	public $workers = [];

	public function init()
	{
		$this->gearman_worker->addServer();
		$this->gearman_client->addServer();
		if (!empty($this->jobNamesPrefix)) {
		    JobNames::$jobPrefix = $this->jobNamesPrefix;
        }
		parent::init();
	}

	/**
	 * @param $worker_id
	 * @param $function
	 * @throws GearmanException
	 */
	public function worker($worker_id, $function)
	{
		$current_worker = NULL;
		if (!empty($worker_id)) {
			$current_worker = new $this->workers[$worker_id]();
		} else {
			throw new GearmanException("No worker found: {$worker_id}");
		}
		/** @var WorkerInterface $current_worker_object */
		$current_worker_object = new $current_worker();
		$function_name = JobNames::make($function, $current_worker_object->get_worker_id());
		$this->gearman_worker->addFunction($function_name, [$current_worker, "worker_" . $function]);
		$this->gearman_worker->work();
	}

	public function addJob($function, $data)
	{
		if ($this->dev_mode) {
			return FALSE;
		}

		$this->gearman_client->addTaskBackground($function, Json::encode($data));
		$this->gearman_client->runTasks();


		return TRUE;
	}

	public function start_work()
	{
		while ($this->gearman_worker->work()) {
		};
	}
}