<?php
namespace Bymorev\components\gearman\exceptions;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 06.06.16
 * Time: 16:55
 */
class GearmanException extends Exception
{
}