<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 06.06.16
 * Time: 16:44
 */

namespace Bymorev\components\gearman\interfaces;

interface WorkerInterface
{
	const WORKER_TELEGRAM = 'Bymorev\components\gearman\workers\TelegramWorker';
	/**
	 * @param array $data
	 * @return mixed
	 */
	public function work(array $data = []);

	public function get_worker_id();
}