<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 06.06.16
 * Time: 16:44
 */

namespace Bymorev\components\gearman\workers;

use Bymorev\components\gearman\exceptions\GearmanException;
use Bymorev\components\gearman\interfaces\WorkerInterface;
use yii\helpers\Json;

class TelegramWorker implements WorkerInterface
{
	const WORKER_ID = "telegram";
	const JOB_MESSAGE = "message";

	/**
	 * @param array $data
	 * @return mixed
	 */
	public function work(array $data = [])
	{
	}

	public function get_worker_id()
	{
		return static::WORKER_ID;
	}

	/**
	 * @param $job \GearmanJob
	 * @throws GearmanException
	 */
	public function worker_message($job)
	{
		$data = Json::decode($job->workload());
		if (empty($data['message'])) {
			throw new GearmanException("No message in workload");
		}
		if (empty($data['who'])) {
			throw new GearmanException("No who in workload");
		}

		\Yii::$app->telegram->message($data['who'],$data['message'], false);
	}
}