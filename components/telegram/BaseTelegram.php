<?php
namespace Bymorev\components\telegram;

use Bymorev\components\gearman\helpers\JobNames;
use Bymorev\components\gearman\workers\TelegramWorker;
use Bymorev\components\telegram\auth\TelegramAuth;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Yii;
use yii\base\Component;
use yii\base\Exception;

class BaseTelegram extends Component
{
    /** @var bool */
    public $api_key = FALSE;

    /** @var Api */
    protected $_telegram;

    /** @var array */
    public $available = [];

    protected $_bot_name = NULL;

    protected $_telegram_auth = NULL;

    protected $_current_telegram_me = NULL;

    public function init()
    {
        if (!$this->api_key) {
            throw new Exception("No api key in telegram");
        }
        $this->_telegram = new Api($this->api_key);
        parent::init();
    }

    public function getBotName()
    {
        if (!$this->_bot_name) {
            $this->_bot_name = $this->_telegram->getMe()->get('username');
        }

        return $this->_bot_name;
    }

    /**
     * @param string|array $who
     * @param bool|string $message
     * @param bool $background
     * @return bool|void
     */
    public function message($who, $message = FALSE, $background = TRUE)
    {
        if ($background) {
            return $this->messageBackground($who, $message);
        }
        $let = FALSE;
        $chat = [];
        if (!is_array($who)) {
            $who = [$who];
        }
        foreach ($this->available as $k => $item) {
            foreach ($who as $w) {
                if ($k == $w) {
                    $chat[] = $item;
                    $let = TRUE;
                }
            }
        }
        foreach ($who as $wh) {
            if (is_numeric($wh)) {
                $chat[] = $wh;
                $let = TRUE;
            }
        }

        if (!$let) {
            Yii::warning("Telegram:: NO AVAILABLE: " . print_r(func_get_args(), 1));

            return FALSE;
        }
        if (!$message) {
            $message = 'Hello World';
        }
        $breaks = array("<br />","<br>","<br/>");
        $message = str_ireplace($breaks, '', trim($message));
        try {
            foreach ($chat as $item) {
                $this->_telegram->sendMessage([
                    'chat_id'    => $item,
                    'text'       => $message,
                    'parse_mode' => 'HTML',
                ]);
            }
        } catch (TelegramSDKException $e) {
            Yii::error("Telegram:: " . $e->getMessage());
        }

        return TRUE;
    }

    public function messageBackground($who, $message = FALSE)
    {
        try {
            $result = Yii::$app->gearman->addJob(JobNames::make(TelegramWorker::JOB_MESSAGE, TelegramWorker::WORKER_ID), [
                'who'     => $who,
                'message' => $message,
            ]);
        } catch (\Exception $e) {
            $result = FALSE;
        }
        if (!$result) {
            $this->message($who, $message, FALSE);
        }
    }

    public function sendMessage($params)
    {
        return $this->_telegram->sendMessage($params);
    }

    /**
     * @return Api
     */
    public function getTelegram()
    {
        return $this->_telegram;
    }

    /**
     * @param array $params
     * @return \Telegram\Bot\Objects\Update[]
     */
    public function getUpdates($params = [])
    {
        return $this->_telegram->getUpdates($params);
    }

    public function getTelegramMe()
    {
        if (!$this->_current_telegram_me) {
            $this->_current_telegram_me = $this->getCurrentTelegramAuth()->getUserTelegramId();
        }

        return $this->_current_telegram_me;
    }

    public function getCurrentTelegramAuth()
    {
        if (is_null($this->_telegram_auth)) {
            $user_id = Yii::$app->user->id;
            $this->_telegram_auth = $user_id ? new TelegramAuth($user_id) : NULL;
        }

        return $this->_telegram_auth;
    }

    public function sendRoleMessage($role, $message)
    {

    }
}