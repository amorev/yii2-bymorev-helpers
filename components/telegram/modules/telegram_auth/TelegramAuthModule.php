<?php

namespace Bymorev\components\telegram\modules\telegram_auth;

/**
 * telegram_auth_module module definition class
 */
class TelegramAuthModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'Bymorev\components\telegram\modules\telegram_auth\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
