<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 16.09.16
 * Time: 11:14
 */
/** @var \yii\web\View $this */
/** @var \common\models\User $user */
/** @var \Bymorev\components\telegram\auth\TelegramAuth $telegramAuth */
/** @var string $invite_link */
$userConnection = $telegramAuth->getUserConnection();
if ($telegramAuth->isUserAuth()) {
    echo $this->render("_is_auth", [
        'userConnection' => $userConnection,
    ]);
    $invite_link = getenv("TELEGRAM_MANAGERS_CHAT_INVITE_LINK");
    if (isset($invite_link)) {
        echo "<BR> Подключайтесь в чат менеджеров<br>". \yii\helpers\Html::a($invite_link, $invite_link);
    }
} else {
    echo $this->render("_not_auth", [
        'userConnection' => $userConnection,
    ]);
}