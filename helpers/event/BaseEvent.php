<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 12.10.16
 * Time: 14:35
 */

namespace Bymorev\helpers\event;

abstract class BaseEvent
{
    protected $_event_data = [];

    protected $_changed = [];

    protected $_insert;

    protected $_old_status = false;

    protected $_current_status;

    /**
     * BaseEvent constructor.
     * @param array $_event_data
     * @param int $insert
     * @param array $changed
     */
    public function __construct(array $_event_data, $insert = 0, $changed = [])
    {
        $this->_event_data = $_event_data;
        $this->_insert = $insert;
        $this->_changed = $changed;
    }


    protected function ComeToStatus($status_id)
    {
        if ($this->_old_status != $status_id && $this->_current_status == $status_id) {
            return TRUE;
        }

        return FALSE;
    }

    protected function isAttributeChanged($attribute, $current_value)
    {
        return (!empty($this->_changed[$attribute]) && $this->_changed[$attribute] != $current_value);
    }
}