<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 10.03.16
 * Time: 22:46
 */

namespace Bymorev\helpers\man\properties;

/**
 * This is the model class for table "st_doctor".
 *
 * @property integer $id
 * @property string  $fname
 * @property string  $mname
 * @property string  $lname
 * @property string  $phone
 * @property string  $email
 * @property string  $ava_base
 * @property string  $ava_path
 * @property string  $date_birth
 * @property string  $comment
 *
 */
class Gender
{
	const MAN = 1;
	const WOMAN = 2;
	public static $genders = [
		1 => [
			'id'=>1,
			'code'     => 'man',
			'fullname'  => 'Мужской',
			'shortname' => "Муж",
		],
		2 => [
			'id'=>2,
			'code'     => 'woman',
			'fullname'  => 'Женский',
			'shortname' => "Жен",
		],
	];

	/**
	 * @param integer $gender
	 * @return Gender
	 */
	public static function gen($gender)
	{
		$gen = new GenderObject(self::$genders[$gender]);
		return $gen;
	}
}