<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 10.03.16
 * Time: 22:53
 */

namespace Bymorev\helpers\man\properties;

class GenderObject
{
	public $code,$fullname,$shortname;
	public $id;

	public function __construct($gender_array)
	{
		foreach ($gender_array as $k=>$item) {
			$this->{$k} = $item;
		}
		return $this;
	}
}