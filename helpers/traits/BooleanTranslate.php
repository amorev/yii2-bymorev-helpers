<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 25.05.16
 * Time: 17:53
 */

namespace Bymorev\helpers\traits;

trait BooleanTranslate
{
	public static $boolean_names = [
		0=>"Нет",
		1=>"Да",
	];
}