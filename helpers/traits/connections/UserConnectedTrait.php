<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 05.10.17
 * Time: 8:48
 */

namespace Bymorev\helpers\traits\connections;

use Bymorev\helpers\misc\AbstractDataConnectionMaker;
use common\models\User;

trait UserConnectedTrait
{
    /**
     * @var User
     */
    private $_user;

    private $_user_id;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return AbstractDataConnectionMaker::getId($this->_user_id, $this->_user);
    }

    /**
     * @param mixed $booking_id
     * @return $this
     */
    public function setUserId($booking_id)
    {
        $this->_user_id = $booking_id;

        return $this;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return AbstractDataConnectionMaker::getObject($this->_user_id, $this->_user, User::class);
    }

    /**
     * @param mixed $order
     * @return $this
     */
    public function setUser(User $order)
    {
        $this->_user = $order;

        return $this;
    }
}