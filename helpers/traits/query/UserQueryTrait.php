<?php
/**
 * Created by PhpStorm.
 * User: zvinger
 * Date: 05.10.17
 * Time: 8:54
 */

namespace Bymorev\helpers\traits\query;

use common\models\service\account\AccountQuery;

trait UserQueryTrait
{
    public function byUser($user_id)
    {
        /** @var AccountQuery $this */
        return $this->andWhere(['user_id' => $user_id]);
    }
}