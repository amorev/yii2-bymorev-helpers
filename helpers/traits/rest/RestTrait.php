<?php
namespace Bymorev\helpers\traits\rest;

use yii\helpers\Json;

trait RestTrait
{
    protected $_rest_input_data = [];
    protected function getInputData($key = NULL)
    {
        if (!$this->_rest_input_data) {
            $php_input = \Yii::$app->request->getRawBody();
            if ($php_input) {
                $this->_rest_input_data = Json::decode($php_input);
            }
        }
        if (!$key) {
            return $this->_rest_input_data;
        }

        return isset($this->_rest_input_data[$key]) ? $this->_rest_input_data[$key] : NULL;
    }
}